/*
 *  Copyright (C) 2012 ammonkey <am.monkeyd@gmail.com>
 *  Copyright (C) 2013 Julián Unrrein <junrrein@gmail.com>
 *
 *  This program or library is free software; you can redistribute it
 *  and/or modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 3 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General
 *  Public License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301 USA.
 */

/**
 * A floating status bar that displays a single line of text.
 *
 * This widget is intended to be used as an overlay for a {@link Gtk.Overlay} and is placed in the
 * bottom-right corner by default. You can change its position like you would do for any overlay
 * widget used in a {@link Gtk.Overlay}.
 *
 * The Overlay Bar displays a single line of text that can be changed using the "status" property.
 *
 * {{../../doc/images/OverlayBar.png}}
 *
 * This widget tries to avoid getting in front of the content being displayed inside the {@link Gtk.Overlay}
 * by moving itself horizontally to the opposite side from the current one when the mouse pointer enters
 * the widget.
 *
 * For this widget to function correctly, the event {@link Gdk.EventMask.ENTER_NOTIFY_MASK} must be set
 * for the parent {@link Gtk.Overlay}. Overlay Bar's constructor takes care of this automatically, if
 * the parent is supplied as a parameter, but you have to be careful not to unset the event for
 * the {@link Gtk.Overlay} at a later stage.
 *
 * @see Gtk.Overlay
 *
 * ''Example''<<BR>>
 * {{{
 * public class OverlayBarView : Gtk.Overlay {
 *     construct {
 *         var button = new Gtk.ToggleButton.with_label ("Show Spinner");
 * 
 *         var grid = new Gtk.Grid ();
 *         grid.halign = Gtk.Align.CENTER;
 *         grid.valign = Gtk.Align.CENTER;
 *         grid.add (button);
 * 
 *         var overlaybar = new Granite.Widgets.OverlayBar (this);
 *         overlaybar.label = "Hover the OverlayBar to change its position";
 *         
 *         add (grid);
 * 
 *         button.toggled.connect (() => {
 *             overlaybar.active = button.active;
 *         });
 *     }
 * }
 * }}}
 */
public class Granite.Widgets.OverlayBar : Gtk.EventBox {

    private const string FALLBACK_THEME = """
        .overlay-bar {
            background-color: alpha (#333, 0.8);
            border-radius: 3px;
            border-width: 0;
            box-shadow:
                0 1px 3px alpha (#000, 0.12),
                0 1px 2px alpha (#000, 0.24);
            color: #fff;
            padding: 3px 6px;
            margin: 6px;
            text-shadow: 0 1px 2px alpha (#000, 0.6);
        }
    """;

    private Gtk.Label status_label;
    private Gtk.Revealer revealer;
    private Gtk.Spinner spinner;

    /**
     * overlay: Deprecated property, always returns null.
     */
    [Version (deprecated = true, deprecated_since = "0.5.1", replacement = "")]
    public Gtk.Overlay? overlay { get; construct; }

    /**
     * Status text displayed inside the Overlay Bar.
     */
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "OverlayBar.label")]
    public string status {
        set {
           status_label.label = value;
        }

        get {
            return status_label.label;
        }
    }

    /**
     * Text displayed inside the Overlay Bar.
     */
    public string label {
        get {
            return status_label.label;
        }
        set {
           status_label.label = value;
        }
    }

    /**
     * Whether to display a {@link Gtk.Spinner} inside the Overlay Bar.
     */
    public bool active {
        get {
            return spinner.active;
        }
        set {
            spinner.active = value;
            revealer.reveal_child = value;
        }
    }
    /**
     * Create a new Overlay Bar, and add it to the {@link Gtk.Overlay}.
     */
    public OverlayBar (Gtk.Overlay? overlay = null) {
        if (overlay != null) {
            overlay.add_events (Gdk.EventMask.ENTER_NOTIFY_MASK);
            overlay.add_overlay (this);
        }
    }

    construct {
        overlay = null;
        status_label = new Gtk.Label ("");
        status_label.set_ellipsize (Pango.EllipsizeMode.END);

        spinner = new Gtk.Spinner ();

        revealer = new Gtk.Revealer ();
        revealer.reveal_child = false;
        revealer.transition_type = Gtk.RevealerTransitionType.SLIDE_LEFT;
        revealer.add (spinner);

        var grid = new Gtk.Grid ();
        grid.add (status_label);
        grid.add (revealer);

        add (grid);

        set_halign (Gtk.Align.END);
        set_valign (Gtk.Align.END);

        int priority = Gtk.STYLE_PROVIDER_PRIORITY_FALLBACK;
        Granite.Widgets.Utils.set_theming (grid, FALLBACK_THEME, StyleClass.OVERLAY_BAR, priority);

        var ctx = grid.get_style_context ();
        var state = ctx.get_state ();

        var padding = ctx.get_padding (state);
        status_label.margin_top = padding.top;
        status_label.margin_bottom = padding.bottom;
        status_label.margin_start = padding.left;
        status_label.margin_end = padding.right;
        spinner.margin_end = padding.right;

        var margin = ctx.get_margin (state);
        grid.margin_top = margin.top;
        grid.margin_bottom = margin.bottom;
        grid.margin_start = margin.left;
        grid.margin_end = margin.right;
    }

    public override void parent_set (Gtk.Widget? old_parent) {
        Gtk.Widget parent = get_parent ();

        if (old_parent != null)
            old_parent.enter_notify_event.disconnect (enter_notify_callback);
        if (parent != null)
            parent.enter_notify_event.connect (enter_notify_callback);
    }

    private bool enter_notify_callback (Gdk.EventCrossing event) {
        if (get_halign () == Gtk.Align.START)
            set_halign (Gtk.Align.END);
        else
            set_halign (Gtk.Align.START);

        queue_resize ();

        return false;
    }
}


/*
 *  Copyright (C) 2012-2017 Granite Developers
 *
 *  This program or library is free software; you can redistribute it
 *  and/or modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 3 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General
 *  Public License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301 USA.
 */

[Version (deprecated = true, deprecated_since = "0.4.2", replacement = "")]
public enum Granite.TextStyle {
    /**
     * Highest level header
     */
    TITLE,

    /**
     * Second highest header
     */
    H1,

    /**
     * Third highest header
     */
    H2,

    /**
     * Fourth Highest Header
     */
    H3;

    /**
     * Converts this to a CSS style string that could be used with e.g: {@link Granite.Widgets.Utils.set_theming}.
     *
     * @param style_class the style class used for this
     *
     * @return CSS of text style
     */
    public string get_stylesheet (out string style_class = null) {
        switch (this) {
            case TITLE:
                style_class = StyleClass.TITLE_TEXT;
                return @".$style_class { font: raleway 36; }";
            case H1:
                style_class = StyleClass.H1_TEXT;
                return @".$style_class { font: open sans bold 24; }";
            case H2:
                style_class = StyleClass.H2_TEXT;
                return @".$style_class { font: open sans light 18; }";
            case H3:
                style_class = StyleClass.H3_TEXT;
                return @".$style_class { font: open sans bold 12; }";
            default:
                assert_not_reached ();
        }
    }
}

/**
 * An enum used to derermine where the window manager currently displays its close button on windows.
 * Used with {@link Granite.Widgets.Utils.get_default_close_button_position}.
 */
public enum Granite.CloseButtonPosition
{
    LEFT,
    RIGHT
}

/**
 * This namespace contains functions to apply CSS stylesheets to widgets.
 */
namespace Granite.Widgets.Utils {
    /**
     * Applies colorPrimary property to the window. The colorPrimary property currently changes
     * the color of the {@link Gtk.HeaderBar} and it's children so that the application window
     * can have a so-called "brand color".
     *
     * Note that this currently only works with the default stylesheet that elementary OS uses.
     *
     * @param window the widget to apply the color, for most cases the widget will be actually the {@link Gtk.Window} itself
     * @param color the color to apply
     * @param priority priorty of change, by default {@link Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION}
     *
     * @return the added {@link Gtk.CssProvider}, or null in case the parsing of
     *         stylesheet failed.
     */
    public Gtk.CssProvider? set_color_primary (Gtk.Widget window, Gdk.RGBA color, int priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION) {
        assert (window != null);

        string hex = color.to_string ();
        return set_theming_for_screen (window.get_screen (), @"@define-color colorPrimary $hex;", priority);
    }

    /**
     * Applies the //stylesheet// to the widget.
     *
     * @param widget widget to apply style to
     * @param stylesheet CSS style to apply to the widget
     * @param class_name class name to add style to, pass null if no class should be applied to the //widget//
     * @param priority priorty of change, for most cases this will be {@link Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION}
     *
     * @return the {@link Gtk.CssProvider} that was applied to the //widget//.
     */
    public Gtk.CssProvider? set_theming (Gtk.Widget widget, string stylesheet,
                              string? class_name, int priority) {
        var css_provider = get_css_provider (stylesheet);

        var context = widget.get_style_context ();

        if (css_provider != null)
            context.add_provider (css_provider, priority);

        if (class_name != null && class_name.strip () != "")
            context.add_class (class_name);

        return css_provider;
    }

    /**
     * Applies a stylesheet to the given //screen//. This will affect all the
     * widgets which are part of that screen.
     *
     * @param screen screen to apply style to, use {@link Gtk.Widget.get_screen} in order to get the screen that the widget is on
     * @param stylesheet CSS style to apply to screen
     * @param priority priorty of change, for most cases this will be {@link Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION}
     *
     * @return the {@link Gtk.CssProvider} that was applied to the //screen//.
     */
    public Gtk.CssProvider? set_theming_for_screen (Gdk.Screen screen, string stylesheet, int priority) {
        var css_provider = get_css_provider (stylesheet);

        if (css_provider != null)
            Gtk.StyleContext.add_provider_for_screen (screen, css_provider, priority);

        return css_provider;
    }

    /**
     * Constructs a new {@link Gtk.CssProvider} that will store the //stylesheet// data.
     * This function uses {@link Gtk.CssProvider.load_from_data} internally so if this method fails
     * then a warning will be thrown and null returned as a result.
     *
     * @param stylesheet CSS style to apply to the returned provider
     *
     * @return a new {@link Gtk.CssProvider}, or null in case the parsing of
     *         //stylesheet// failed.
     */
    public Gtk.CssProvider? get_css_provider (string stylesheet) {
        Gtk.CssProvider provider = new Gtk.CssProvider ();

        try {
            provider.load_from_data (stylesheet, -1);
        }
        catch (Error e) {
            warning ("Could not create CSS Provider: %s\nStylesheet:\n%s",
                     e.message, stylesheet);
            return null;
        }

        return provider;
    }

    /**
     * Determines if the widget should be drawn from left to right or otherwise.
     *
     * @return true if the widget should be drawn from left to right, false otherwise.
     */
    internal bool is_left_to_right (Gtk.Widget widget) {
        var dir = widget.get_direction ();
        if (dir == Gtk.TextDirection.NONE)
            dir = Gtk.Widget.get_default_direction ();
        return dir == Gtk.TextDirection.LTR;
    }

    /**
     * This method applies given text style to given label
     *
     * @param text_style text style to apply
     * @param label label to apply style to
     */
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "")]
    public void apply_text_style_to_label (TextStyle text_style, Gtk.Label label) {
        var style_provider = new Gtk.CssProvider ();
        var style_context = label.get_style_context ();

        string style_class, stylesheet;
        stylesheet = text_style.get_stylesheet (out style_class);
        style_context.add_class (style_class);

        try {
            style_provider.load_from_data (stylesheet, -1);
        } catch (Error err) {
            warning ("Couldn't apply style to label: %s", err.message);
            return;
        }

        style_context.add_provider (style_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
    }

    const string WM_SETTINGS_PATH = "org.gnome.desktop.wm.preferences";
    const string PANTHEON_SETTINGS_PATH = "org.pantheon.desktop.gala.appearance";
    const string WM_BUTTON_LAYOUT_KEY = "button-layout";

    /**
     * This method detects the close button position as configured for the window manager. If you
     * need to know when this key changed, it's best to listen on the schema returned by
     * {@link Granite.Widgets.Utils.get_button_layout_schema} for changes and then call this method again.
     *
     * @param position a {@link Granite.CloseButtonPosition} indicating where to best put the close button
     * @return if no schema was detected by {@link Granite.Widgets.Utils.get_button_layout_schema}
     *         or there was no close value in the button-layout string, false will be returned. The position
     *         will be LEFT in that case.
     */
    public bool get_default_close_button_position (out CloseButtonPosition position) {
        // default value
        position = CloseButtonPosition.LEFT;

        var schema = get_button_layout_schema ();
        if (schema == null) {
            return false;
        }

        var layout = new Settings (schema).get_string (WM_BUTTON_LAYOUT_KEY);
        var parts = layout.split (":");

        if (parts.length < 2) {
            return false;
        }

        if ("close" in parts[0]) {
            position = CloseButtonPosition.LEFT;
            return true;
        } else if ("close" in parts[1]) {
            position = CloseButtonPosition.RIGHT;
            return true;
        }

        return false;
    }

    /**
     * This methods returns the schema used by {@link Granite.Widgets.Utils.get_default_close_button_position}
     * to determine the close button placement. It will first check for the pantheon/gala schema and then fallback
     * to the default gnome one. If neither is available, null is returned. Make sure to check for this case,
     * as otherwise your program may crash on startup.
     *
     * @return the schema name. If the layout could not be determined, a warning will be thrown and null will be returned
     */
    public string? get_button_layout_schema () {
        var sss = SettingsSchemaSource.get_default ();

        if (sss != null) {
            if (sss.lookup (PANTHEON_SETTINGS_PATH, true) != null) {
                return PANTHEON_SETTINGS_PATH;
            } else if (sss.lookup (WM_SETTINGS_PATH, true) != null) {
                return WM_SETTINGS_PATH;
            }
        }

        warning ("No schema indicating the button-layout is installed.");
        return null;
    }
}



/*
 *  Copyright (C) 2012-2017 elementary LLC. (https://elementary.io)
 *
 *  This program or library is free software; you can redistribute it
 *  and/or modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 3 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General
 *  Public License along with this library; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301 USA.
 */

namespace Granite.StyleClass {
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "Granite.STYLE_CLASS_BADGE")]
    public const string BADGE = "badge";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "Granite.STYLE_CLASS_CATEGORY_EXPANDER")]
    public const string CATEGORY_EXPANDER = "category-expander";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "")]
    public const string CONTENT_VIEW = "content-view";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "")]
    public const string CONTENT_VIEW_WINDOW = "content-view-window";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "")]
    public const string COMPOSITED = "composited";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "")]
    public const string DECORATED_WINDOW = "decorated-window";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "Granite.STYLE_CLASS_H1_LABEL")]
    public const string H1_TEXT = "h1";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "Granite.STYLE_CLASS_H2_LABEL")]
    public const string H2_TEXT = "h2";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "Granite.STYLE_CLASS_H3_LABEL")]
    public const string H3_TEXT = "h3";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "")]
    public const string HELP_BUTTON = "help_button";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "Granite.STYLE_CLASS_OVERLAY_BAR")]
    public const string OVERLAY_BAR = "overlay-bar";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "Gtk.STYLE_CLASS_POPOVER")]
    public const string POPOVER = "popover";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "")]
    public const string POPOVER_BG = "popover_bg";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "Granite.STYLE_CLASS_SOURCE_LIST")]
    public const string SOURCE_LIST = "source-list";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "")]
    public const string THIN_PANE_SEPARATOR = "sidebar-pane-separator";
    [Version (deprecated = true, deprecated_since = "0.4.2", replacement = "Gtk.STYLE_CLASS_TITLE")]
    public const string TITLE_TEXT = "title";
}

namespace Granite {
    /**
     * Style class to give accent color to a {@link Gtk.Label} or symbolic icon
     */
    public const string STYLE_CLASS_ACCENT = "accent";
    public const string STYLE_CLASS_AVATAR = "avatar";
    /**
     * Style class for shaping a {@link Gtk.Button}
     */
    public const string STYLE_CLASS_BACK_BUTTON = "back-button";
    /**
     * Style class for numbered badges as in a {@link Granite.Widgets.SourceList}
     */
    public const string STYLE_CLASS_BADGE = "badge";
    /**
     * Style class for adding a small shadow to a container such as for image thumbnails
     *
     * Can be combined with the style class ".collapsed" to further reduce the size of the shadow
     */
    public const string STYLE_CLASS_CARD = "card";
    public const string STYLE_CLASS_CATEGORY_EXPANDER = "category-expander";
    /**
     * Style class for large primary text as seen in {@link Granite.Widgets.Welcome}
     */
    public const string STYLE_CLASS_H1_LABEL = "h1";
    /**
     * Style class for large seondary text as seen in {@link Granite.Widgets.Welcome}
     */
    public const string STYLE_CLASS_H2_LABEL = "h2";
    /**
     * Style class for small primary text
     */
    public const string STYLE_CLASS_H3_LABEL = "h3";
    /**
     * Style class for a {@link Granite.HeaderLabel}
     */
    public const string STYLE_CLASS_H4_LABEL = "h4";
    /**
     * Style class for a {@link Gtk.Switch} used to change between two modes rather than active and inactive states
     */
    public const string STYLE_CLASS_MODE_SWITCH = "mode-switch";
    /**
     * Style class for a {@link Granite.Widgets.OverlayBar}
     */
    public const string STYLE_CLASS_OVERLAY_BAR = "overlay-bar";
    /**
     * Style class for primary label text in a {@link Granite.MessageDialog}
     */
    public const string STYLE_CLASS_PRIMARY_LABEL = "primary";
    /**
     * Style class for a {@link Granite.SeekBar}
     */
    public const string STYLE_CLASS_SEEKBAR = "seek-bar";
    /**
     * Style class for a {@link Granite.Widgets.SourceList}
     */
    public const string STYLE_CLASS_SOURCE_LIST = "source-list";
    /**
     * Style class for a {@link Granite.Widgets.Granite.Widgets.StorageBar}
     */
    public const string STYLE_CLASS_STORAGEBAR = "storage-bar";
    /**
     * Style class for {@link Gtk.Label} or {@link Gtk.TextView} to emulate the appearance of Terminal. This includes
     * text color, background color, selection highlighting, and selecting the system monospace font.
     *
     * When used with {@link Gtk.Label} this style includes internal padding. When used with {@link Gtk.TextView}
     * interal padding will need to be set with {@link Gtk.Container.border_width}
     */
    public const string STYLE_CLASS_TERMINAL = "terminal";
    /**
     * Style class for a {@link Granite.Widgets.Welcome}
     */
    public const string STYLE_CLASS_WELCOME = "welcome";
}


/*-
 * Copyright (c) 2016-2017 elementary LLC. (https://elementary.io)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Artem Anufrij <artem.anufrij@live.de>
 *              Daniel Foré <daniel@elementary.io>
 *
 */

namespace Granite.Widgets {
    /**
     * Toasts are small in-app notifications that provide feedback about an operation
     * in a small popup. They only fill the space required to show the message and do
     * not block the UI.
     *
     * Granite.Widgets.Toast will get the style class .app-notification
     *
     * {{../../doc/images/Toast.png}}
     */
    public class Toast : Gtk.Revealer {

        /**
         * Emitted when the Toast is closed by activating the close button
         */
        public signal void closed ();

        /**
         * Emitted when the default action button is activated
         */
        public signal void default_action ();

        private Gtk.Label notification_label;
        private Gtk.Button default_action_button;
        private string _title;
        private uint timeout_id;

        /**
         * The notification text label to be displayed inside of #this
         */
        public string title {
            get {
                return _title;
            }
            construct set {
                if (notification_label != null) {
                    notification_label.label = value;
                }
                _title = value;
            }
        }

        /**
         * Creates a new Toast with #title as its title
         */
        public Toast (string title) {
            Object (title: title);
        }

        construct {
            margin = 3;
            halign = Gtk.Align.CENTER;
            valign = Gtk.Align.START;

            default_action_button = new Gtk.Button ();
            default_action_button.visible = false;
            default_action_button.no_show_all = true;
            default_action_button.clicked.connect (() => {
                reveal_child = false;
                if (timeout_id != 0) {
                    Source.remove (timeout_id);
                    timeout_id = 0;
                }
                default_action ();
            });

            var close_button = new Gtk.Button.from_icon_name ("close-symbolic", Gtk.IconSize.MENU);
            close_button.get_style_context ().add_class ("close-button");
            close_button.clicked.connect (() => {
                reveal_child = false;
                if (timeout_id != 0) {
                    Source.remove (timeout_id);
                    timeout_id = 0;
                }
                closed ();
            });

            notification_label = new Gtk.Label (title);

            var notification_box = new Gtk.Grid ();
            notification_box.column_spacing = 12;
            notification_box.add (close_button);
            notification_box.add (notification_label);
            notification_box.add (default_action_button);

            var notification_frame = new Gtk.Frame (null);
            notification_frame.get_style_context ().add_class ("app-notification");
            notification_frame.add (notification_box);

            add (notification_frame);
        }

        /**
         * Sets the default action button label of #this to #label and hides the
         * button if #label is #null.
         */
        public void set_default_action (string? label) {
            if (label == "" || label == null) {
                default_action_button.no_show_all = true;
                default_action_button.visible = false;
            } else {
                default_action_button.no_show_all = false;
                default_action_button.visible = true;
            }
            default_action_button.label = label;
        }

        /**
         * Sends the Toast on behalf of #this
         */
        public void send_notification () {
            if (!child_revealed) {
                reveal_child = true;

                uint duration;

                if (default_action_button.visible) {
                    duration = 3500;
                } else {
                    duration = 2000;
                }

                timeout_id = GLib.Timeout.add (duration, () => {
                    reveal_child = false;
                    timeout_id = 0;
                    return false;
                });
            }
        }
    }
}