public class MyApp : Gtk.Application {

public MyApp () {
	Object (
		application_id: "ro.psnszsn.simplernotes",
		flags : ApplicationFlags.FLAGS_NONE
		);
}

LocalNotes notez = null;
bool is_saving = false;
Gtk.ApplicationWindow window0;
Granite.Widgets.Toast toast;
Gtk.ToggleButton trash_button;
Gtk.SearchEntry search_entry;
Gtk.SearchBar search_bar;

protected override void activate () {
	var settings = new Settings ("ro.psnszsn.simplernotes");

	if(settings.get_string ("mail")!="")
		notez = new LocalNotes(new SimpleNote(settings.get_string ("mail"), settings.get_string ("pass")));

	var builder = new Gtk.Builder ();
	try {
		builder.add_from_resource ("/ro/panazan/vlad/simplernotes/layout.glade");
	}
	catch (Error e) {
		error ("Unable to load file: %s", e.message);
	}

	window0 = builder.get_object ("mainwindow") as Gtk.ApplicationWindow;
	window0.application = this;
	var about_dialog = builder.get_object ("about_dialog") as Gtk.AboutDialog;
	var headerbar = builder.get_object ("header") as Gtk.HeaderBar;
	var overlay = builder.get_object ("overlay") as Gtk.Overlay;
	var note_stack = builder.get_object ("stack1") as Gtk.Stack;
	var note_sidebar = builder.get_object ("sidebar") as Gtk.StackSidebar;
	var add_button = builder.get_object ("add") as Gtk.Button;
	var sync_button = builder.get_object ("sync") as Gtk.Button;
	var about_button = builder.get_object ("about_button") as Gtk.Button;
	var search_button = builder.get_object ("search_button") as Gtk.Button;
	var delete_button = builder.get_object ("delete_button") as Gtk.Button;
	var sign_out_button = builder.get_object ("sign_out_button") as Gtk.Button;
	var sign_in_stack = builder.get_object ("sign_in_stack") as Gtk.Grid;
	var sign_in_button = builder.get_object ("sign_in") as Gtk.Button;
	var email = builder.get_object ("email") as Gtk.Entry;
	var pass = builder.get_object ("pass") as Gtk.Entry;

	var restore_button = new Gtk.Button.with_label ("Restore note");
	var delete4ever_button = new Gtk.Button.with_label ("Delete forever");

	var info_toggle = builder.get_object ("info_toggle") as Gtk.ToggleButton;
	search_bar = builder.get_object ("search_bar") as Gtk.SearchBar;
	search_entry = builder.get_object ("search_entryz") as Gtk.SearchEntry;
	trash_button = builder.get_object ("trash_button") as Gtk.ToggleButton;
	toast = new Granite.Widgets.Toast ("Sign in failed...");
	overlay.add_overlay(toast);

	foreach (Gtk.Widget element in note_stack.get_children ()) note_stack.remove (element);

	window0.delete_event.connect (() => {
			print("\nExiting app.");
			new Thread<void*> (null, () => {
				//  Thread.usleep(2000000);
				notez.sync_from_list();
				is_saving = false;
				print("\nByeee!");
				this.quit();
				return null;
			});
			return window0.hide_on_delete ();
			//  return false;
		});

	about_dialog.delete_event.connect (about_dialog.hide_on_delete);

	add_button.clicked.connect (() => {
			notez.add_note();
			if(trash_button.get_active()==true) trash_button.set_active (false);
			render_notes(note_stack);
			note_stack.show_all ();
		});

	search_button.clicked.connect (() => {
			if(search_bar.get_search_mode()) search_bar.set_search_mode(false); else search_bar.set_search_mode(true);

		});

	info_toggle.toggled.connect (() => {
			var revv = (note_stack.get_visible_child () as Gtk.Grid ).get_children().nth(1).data as Gtk.Revealer;
			if(info_toggle.get_active()) revv.set_reveal_child(true); else revv.set_reveal_child(false);

		});
	search_entry.search_changed.connect (() => {
			print(search_entry.get_text());
			render_notes(note_stack);
			headerbar.show_all();
			note_stack.show_all ();

		});

	sync_button.clicked.connect (() => {

			var overlaybar = new Granite.Widgets.OverlayBar(overlay);
			overlaybar.label="Sync in progress";
			overlaybar.active=true;
			overlaybar.show_all();
			sync_all.begin (note_stack,(obj, res) => {
				sync_all.end (res);
				render_notes(note_stack);
				note_stack.show_all ();
				print ("Sync done.\n");
				//  overlaybar.set_visible(false);
				overlay.remove(overlaybar);
			});
		});

	sign_in_button.clicked.connect (() => {
			uint return_code;
			notez = new LocalNotes(new SimpleNote(email.get_text(), pass.get_text()));

			notez.instanta.get_token(out return_code);
			if (return_code==200) {
			        settings.set_string ("mail", email.get_text());
			        settings.set_string ("pass", pass.get_text());

			        notez.load_list();
			        render_notes(note_stack);
			        window0.show_all ();
			        sync_button.clicked();
			}else{
			        toast.send_notification ();
			}
			//  print ("Status Code: %u\n", return_code);
		});

	sign_out_button.clicked.connect (() => {

			new Thread<void*> (null, () => {
				notez.empty_data_folder();
				notez = null;
				return null;
			});

			foreach (Gtk.Widget element in note_stack.get_children ()) note_stack.remove (element);
			settings.set_string ("mail", "");
			settings.set_string ("pass", "");

			note_stack.add_titled (sign_in_stack, "sign-in", "Sign in");
		});

	about_button.clicked.connect (() => {
			about_dialog.present();
		});

	delete_button.clicked.connect (() => {
			int selected_note_id = notez.get_note_id_by_key(note_stack.get_visible_child_name());
			print(notez.list[selected_note_id].deleted.to_string());
			var now=new DateTime.now_local ().to_unix();

			notez.list[selected_note_id].modifydate = now;
			notez.list[selected_note_id].deleted = 1;
			render_notes(note_stack);
			note_stack.show_all ();

			new Thread<void*> (null, () => {
				notez.update_note_local(notez.list[selected_note_id]);
				notez.update_note_cloud(notez.list[selected_note_id]);
				return null;
			});
		});

	trash_button.toggled.connect (() => {


			if (trash_button.active) {
			        render_notes(note_stack);
			        delete_button.set_visible(false);
			        headerbar.remove(delete_button);
			        headerbar.pack_end(restore_button);
			        headerbar.pack_end(delete4ever_button);
			        headerbar.set_subtitle("Trash");
			} else {
			        render_notes(note_stack);
			        delete_button.set_visible(true);
			        headerbar.pack_end(delete_button);
			        headerbar.remove(restore_button);
			        headerbar.remove(delete4ever_button);
			        headerbar.set_subtitle(null);
			}

			headerbar.show_all();
			note_stack.show_all ();

		});

	delete4ever_button.clicked.connect (() => {
			int selected_note_id = notez.get_note_id_by_key(note_stack.get_visible_child_name());


			perm_delete.begin (selected_note_id,(obj, res) => {
				perm_delete.end (res);
				render_notes(note_stack);
				note_stack.show_all ();
				print ("Note deleted.\n");
			});

			render_notes(note_stack);
			note_stack.show_all ();
		});

	restore_button.clicked.connect (() => {
			int selected_note_id = notez.get_note_id_by_key(note_stack.get_visible_child_name());
			print(notez.list[selected_note_id].deleted.to_string());
			var now=new DateTime.now_local ().to_unix();

			notez.list[selected_note_id].modifydate = now;
			notez.list[selected_note_id].deleted = 0;
			render_notes(note_stack);
			note_stack.show_all ();

			new Thread<void*> (null, () => {
				notez.update_note_local(notez.list[selected_note_id]);
				notez.update_note_cloud(notez.list[selected_note_id]);
				return null;
			});
		});

	note_sidebar.set_focus_child.connect (() => {
			if(note_sidebar.get_focus_child()!=null) info_toggle.toggled();
		});

	if(notez!=null) {
		notez.load_list();
		render_notes(note_stack);
	}else{
		note_stack.add_titled (sign_in_stack, "sign-in", "Sign in");
	}

	window0.show_all ();
}

async void perm_delete(int selected_note_id) {
	new Thread<void*> (null, () => {
			if(notez.list[selected_note_id].deleted == 1)
				notez.delete_note_cloud(notez.list[selected_note_id]);

			Idle.add (perm_delete.callback);
			return null;
		});
	yield;
}
async void sync_all(Gtk.Stack note_stack) {
	new Thread<void*> (null, () => {
			if(notez==null) {toast.send_notification (); Idle.add (sync_all.callback); return null;}
			if(notez.sync_from_list()==1)
				toast.send_notification ();
			else
				notez.save_list();
			is_saving = false;
			Idle.add (sync_all.callback);
			return null;
		});
	yield;
}

Gtk.Grid render_note_box(Note i, out string title, Gtk.Stack note_stack){

	string content = i.content;


	var builder3 = new Gtk.Builder ();
	builder3.add_objects_from_resource ("/ro/panazan/vlad/simplernotes/layout.glade", {"note_box"});

	var inner_box = builder3.get_object ("note_box") as Gtk.Grid;
	var reve  = builder3.get_object ("reve") as Gtk.Revealer;
	var flow= builder3.get_object ("tagflow") as Gtk.FlowBox;
	var text_notita =  builder3.get_object ("note_text") as Gtk.TextView;
	var tag_add =  builder3.get_object ("add_tag_entry") as Gtk.Entry;

	var create_label =  builder3.get_object ("create") as Gtk.Label;
	var modify_label =  builder3.get_object ("modify") as Gtk.Label;
	var word_label =  builder3.get_object ("words") as Gtk.Label;
	var char_label =  builder3.get_object ("chars") as Gtk.Label;


	flow.set_max_children_per_line(100);
	flow.set_row_spacing(5);


	create_label.label = new DateTime.from_unix_local(i.createdate).format("%x %X");
	modify_label.label = new DateTime.from_unix_local(i.modifydate).format("%x %X");
	char_label.label = i.content.char_count().to_string() + " characters";

	int wc=GLib.Regex.split_simple("\\w+",i.content).length-1;
	if (i.content=="") wc=0;
	word_label.label = wc.to_string() + " words";


	//  foreach (string y in i.tags.split(" ")) {
	//      var tag=y;
	//      flow.insert(new Gtk.Label(tag),-1);
	//  }
	for (int a = 0; a < i.tags.split(" ").length; a++) {
		var tag=i.tags.split(" ")[a];
		flow.insert(new Gtk.Label(tag),a);
	}

	//  Gtk.Entry tag_add = new Gtk.Entry ();
	//  flow.insert(tag_add,-1);

	var grid = new Gtk.Grid ();
	grid.orientation = Gtk.Orientation.VERTICAL;
	var blue = Gdk.RGBA ();
	blue.parse ("rgba (1, 1, 1, 0)");
	//  tag_add.set_has_frame(false);
	//  tag_add.max_width_chars=2;
	//  tag_add.set_placeholder_text("Add tagz...");

	tag_add.override_background_color(Gtk.StateFlags.NORMAL, blue);
	tag_add.changed.connect(() => {
			tag_add.set_width_chars(tag_add.buffer.text.char_count());
		});
	tag_add.focus_out_event.connect(() => {
			tag_add.set_text ("");
			tag_add.set_width_chars(7);
			return false;
		});
	tag_add.activate.connect(() => {
			var newtags=0;
			foreach (string y in tag_add.buffer.text.split(" ")) {
			        var tag=y;
			        if(!i.tags.contains(tag)) {
			                var num_children=(int)flow.get_children().length();
			                flow.insert(new Gtk.Label(tag),num_children-1);
			                if(i.tags != "") i.tags+=" ";
			                i.tags+=tag;
			                newtags++;
				}
			}
			tag_add.set_text ("");

			if(newtags!=0) {
			        flow.show_all();
			        i.modifydate=new DateTime.now_local ().to_unix();
			        if(notez.list.index_of(i)!=0) {
			                notez.list.remove(i);
			                notez.list.insert (0,i);
				}

			        new Thread<void*> (null, () => {
					notez.update_note_local(i);
					notez.update_note_cloud(i);
					return null;
				});
			}
		});
	var tag_delete_button = new Gtk.Button.with_label ("Delete");
	Gtk.FlowBoxChild flowchild=null;
	tag_delete_button.clicked.connect (() => {
			//  flowchild.remove(flowchild.get_child ());
			flow.remove(flowchild);
			var tagval = ((Gtk.Label)flowchild.get_child ()).get_label();
			i.tags=i.tags.replace(tagval, "");
			i.tags=i.tags.replace("  ", " ");
			i.tags=i.tags.strip ();
			i.modifydate=new DateTime.now_local ().to_unix();

			new Thread<void*> (null, () => {
				notez.update_note_local(i);
				notez.update_note_cloud(i);
				return null;
			});
		});
	grid.add (tag_delete_button);



	var menu_popover = new Gtk.Popover(null);
	menu_popover.position = Gtk.PositionType.BOTTOM;
	menu_popover.modal = false;
	menu_popover.add_with_properties(grid);
	menu_popover.show_all();

	flow.child_activated.connect ((copil) => {
			flowchild=copil;
			if(copil.get_child().get_type().name ()=="GtkEntry") {flow.unselect_child(copil); return;};

			if(menu_popover.get_visible() ==true & menu_popover.get_relative_to()==copil) {
			        menu_popover.set_visible (false);
			        flow.unselect_child(copil);
			}
			else{
			        //  menu_popover.show_all();
			        menu_popover.set_relative_to(copil);
			        menu_popover.popup ();
			}

		});


	text_notita.set_wrap_mode(Gtk.WrapMode.WORD);
	text_notita.set_left_margin(70);
	text_notita.set_right_margin(70);
	text_notita.set_top_margin(50);

	text_notita.buffer.text = content;
	text_notita.buffer.changed.connect (() => {
			i.content=text_notita.buffer.text;
			var now=new DateTime.now_local ().to_unix();
			if(notez.list.index_of(i)!=0) {
			        notez.list.remove(i);
			        notez.list.insert (0,i);
			}
			i.modifydate=now;

			wc=GLib.Regex.split_simple("\\w+",i.content).length-1;
			if (i.content=="") wc=0;
			word_label.label = wc.to_string() + " words";
			modify_label.label = new DateTime.from_unix_local(i.modifydate).format("%x %X");
			char_label.label = i.content.char_count().to_string() + " characters";


			if(is_saving==false) {
			        new Thread<void*> (null, () => {
					is_saving=true;
					Thread.usleep(20000000);
					if(is_saving==true) {
					        notez.update_note_local(i);
					        notez.update_note_cloud(i);
					}
					is_saving=false;
					return null;
				});
			}
		});
	text_notita.buffer.end_user_action.connect (() => {
			print(".");
		});
	if(content!="")
		title= content.split("\n")[0].length<30 ? content.split("\n")[0] : content.split("\n")[0].substring(0,27)+"...";
	else
		title="New note";
	return inner_box;

}
public void render_notes(Gtk.Stack note_stack) {
	if (notez==null) return;
	var children = note_stack.get_children ();
	foreach (Gtk.Widget element in children)
		note_stack.remove (element);

	if (trash_button.active) {
		foreach (Note i in notez.list) {
			if(i.deleted==1) {
				if(!search_bar.get_search_mode())
				{
					string title;
					var inner_box = render_note_box(i,out title, note_stack);
					note_stack.add_titled (inner_box, i.key, title);
				}else if(i.content.contains(search_entry.get_text()) || i.tags.contains(search_entry.get_text())) {
					string title;
					var inner_box = render_note_box(i,out title, note_stack);
					note_stack.add_titled (inner_box, i.key, title);
				}
			}
		}
	} else {
		foreach (Note i in notez.list) {
			if(i.deleted==0) {
				if(!search_bar.get_search_mode())
				{
					string title;
					var inner_box = render_note_box(i,out title, note_stack);
					note_stack.add_titled (inner_box, i.key, title);
				}else if(i.content.contains(search_entry.get_text()) || i.tags.contains(search_entry.get_text())) {
					string title;
					var inner_box = render_note_box(i,out title, note_stack);
					note_stack.add_titled (inner_box, i.key, title);
				}
			}
		}
	}


}


public static int main (string[] args) {
	if (!Thread.supported ()) {
		stderr.printf ("Cannot run without thread support.\n");
		return 1;
	}
	var app = new MyApp ();
	return app.run (args);
}
}


