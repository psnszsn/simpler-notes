class SimpleNote : Object {
string AUTH_URL = "https://app.simplenote.com/api/login";
string DATA_URL ="https://app.simplenote.com/api2/data";
string INDX_URL = "https://app.simplenote.com/api2/index?";
int NOTE_FETCH_LENGTH = 100;
string username;
string password;
public string token = null;

public SimpleNote(string user, string pass) {
	this.username = user;
	this.password = pass;
}

public string authenticate(string user, string pass, out uint return_code = null){

	string auth_params = "email="+user+"&password="+pass;
	string auth_encoded = Base64.encode (auth_params.data);

	var sessionp = new Soup.Session ();
	var messagep = new Soup.Message ("POST", AUTH_URL);
	messagep.set_request("text/plain", Soup.MemoryUse.COPY, auth_encoded.data);
	sessionp.send_message (messagep);
	return_code = messagep.status_code;
	return (string)messagep.response_body.data;

}

public string get_token(out uint return_code = null){

	if (this.token == null) {
		this.token = this.authenticate(this.username, this.password, out return_code);
		//  print ("Status Code: %u\n", return_code);
	}
	//  stdout.printf(token);
	return this.token;

}

public Json.Object ? get_note(string noteid){

	string totoken = this.get_token();
	string params = "/"+noteid+"?auth="+totoken+"&email="+username;
	//  stdout.printf("\n");
	//  stdout.printf(DATA_URL+params);
	//  stdout.printf("\n");
	var session = new Soup.Session();
	var message = new Soup.Message("GET", DATA_URL+params);
	session.send_message(message);

	try {
		var parser = new Json.Parser();
		parser.load_from_data ((string) message.response_body.flatten ().data, -1);

		var root_object = parser.get_root().get_object ();
		return root_object;

	} catch (Error e) {
		stderr.printf ("I guess something is not working...\n");
		return null;
	}



}


public Json.Object ? update_note(string noteid, string notejson){

	//  stdout.printf("\n");
	//  print (notejson);
	//  stdout.printf("\n");

	string totoken = this.get_token();
	string params_update = "/"+noteid+"?auth="+totoken+"&email="+username;
	string params_add = "?auth="+totoken+"&email="+username;
	string params;
	noteid.split(".")[0]=="temp" ? params=params_add : params=params_update;

	var sessionp = new Soup.Session ();
	var messagep = new Soup.Message ("POST", DATA_URL+params);

	messagep.set_request("text/plain", Soup.MemoryUse.COPY, notejson.data);
	sessionp.send_message (messagep);

	//  print ((string)messagep.response_body.data);

	//  return (string)messagep.response_body.data;


	stdout.printf("\n");
	//  stdout.printf(DATA_URL+params);

	try {
		var parser = new Json.Parser();
		parser.load_from_data ((string) messagep.response_body.flatten ().data, -1);

		var root_object = parser.get_root().get_object ();
		return root_object;

	} catch (Error e) {
		stderr.printf ("I guess something is not working...\n");
		return null;
	}
}

public int ? delete_note(string noteid){

	stdout.printf("\n");

	string totoken = this.get_token();
	string params = "/"+noteid+"?auth="+totoken+"&email="+username;

	var sessionp = new Soup.Session ();
	var messagep = new Soup.Message ("DELETE", DATA_URL+params);

	sessionp.send_message (messagep);

	//  print ((string)messagep.response_body.data);

	if(messagep.status_code==200)
		return 0; else
		return 1;
	//  return (string)messagep.response_body.data;


}

public Json.Object ? get_note_list(out uint return_code = null){

	string totoken = this.get_token();
	string params = "auth="+totoken+"&email="+username+"&length="+NOTE_FETCH_LENGTH.to_string();

	var session = new Soup.Session ();
	var message = new Soup.Message ("GET", INDX_URL+params);
	session.send_message (message);
	return_code = message.status_code;
	if(return_code==200)
	{
		try {
			var parser = new Json.Parser();
			parser.load_from_data ((string) message.response_body.flatten ().data, -1);

			var root_object = parser.get_root().get_object ();
			return root_object;

		} catch (Error e) {
			stderr.printf ("I guess something is not working...\n");
			return null;
		}
	}else{
		return null;
	}


}


}