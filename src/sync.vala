class Note : Object {
public string key { get; set; }
public string content { get; set; }
public int64 modifydate { get; set; }
public int64 createdate { get; set; }
public string tags { get; set; }
public int64 version { get; set; }
public int64 deleted { get; set; }
public bool istemp { get; set; }

public Note.temp () {
	var dt=new DateTime.now_local ().to_unix();
	this.key = "temp."+dt.to_string()+Random.int_range(1000,9999).to_string();
	this.content = "";
	this.modifydate = dt;
	this.createdate = dt;
	this.tags = "";
	this.version = 1;
	this.istemp=true;
	print("\nNew temp note.");
}
public Note(string key,string content, int64 modifydate, int64 createdate, string tags, int64 version, int64 deleted) {
	this.key = key;
	this.content = content;
	this.modifydate = modifydate;
	this.createdate = createdate;
	this.tags = tags;
	this.version = version;
	this.deleted = deleted;
	this.istemp=false;
	//  print("nota creata");
}
//  public Note clone(){
//  	return new Note (key, content, modifydate, createdate, tags, version, deleted);
//  }
}


class LocalNotes : Object {

public string datafolder = GLib.Environment.get_user_data_dir() + "/simplernotes/notes";
public SimpleNote instanta;
public Gee.ArrayList<Note> list = new Gee.ArrayList<Note> ();

public LocalNotes(SimpleNote x) {
	this.instanta = x;
}

public int save_list(){
	print ("\nList save started...\n");
	empty_data_folder();
	foreach (Note i in list) {
		string data = Json.gobject_to_data (i, null);
		var output = FileStream.open(datafolder+"/"+i.key+".file","w");
		output.printf(data);
		//  print (data);
		//  print ("\n");
	}
	print ("List saved.\n");
	return 0;
}

public int empty_data_folder(){
	try {
		var directory = File.new_for_path (datafolder);
		var enumerator = directory.enumerate_children (FileAttribute.STANDARD_NAME, 0);

		FileInfo file_info;
		while ((file_info = enumerator.next_file ()) != null) {
			File file = File.new_for_path (datafolder+"/"+file_info.get_name());
			try {
				file.delete ();
			} catch (Error e) {
				print ("Error: %s\n", e.message);
			}
		}
		return 0;
	} catch (Error e) {
		stderr.printf ("Error: %s\n", e.message);
		return 1;
	}
}

public int load_list(){
	print ("\nList load started...\n");
	list.clear();
	try {
		var directory = File.new_for_path (datafolder);
		if (!directory.query_exists ()) {
			stdout.printf ("Dir '%s' doesn't exist.\n", directory.get_path ());
			directory.make_directory_with_parents ();
		}
		var enumerator = directory.enumerate_children (FileAttribute.STANDARD_NAME, 0);
		FileInfo file_info;
		while ((file_info = enumerator.next_file ()) != null) {
			string data="";
			var stream = FileStream.open(datafolder+"/"+file_info.get_name(), "r");
			assert (stream != null);
			char buf[100];
			while (stream.gets (buf) != null) {
				data = data+(string) buf;
			}
			Note obj = Json.gobject_from_data (typeof (Note), data) as Note;
			int z = 0;
			foreach (Note i in list) {
				if(i.modifydate>obj.modifydate) z++;
			}
			list.insert (z,obj);
		}
		print ("List loaded.\n");
	} catch (Error e) {
		stderr.printf ("Error: %s\n", e.message);
		return 1;
	}
	print( list.size.to_string()+" notes \n");
	return 0;
}

//  public void print_note_list(){
//      var results = instanta.get_note_list().get_array_member ("data");
//      foreach (var geonode in results.get_elements ()) {
//              var geoname = geonode.get_object ();
//              if(geoname.get_int_member ("deleted")==0) {
//                      string tags = "";
//                      geoname.get_array_member("tags").foreach_element((a,b,c) => {tags+=a.get_string_element(b);});
//                      stdout.printf ("%s\n%s\n%lld\n%s\n--------------\n\n",
//                                     geoname.get_string_member ("key"),
//                                     tags=="" ? "no-tags" : tags,
//                                     geoname.get_int_member ("deleted"),
//                                     geoname.get_string_member("modifydate"));
//              }
//      }
//      print("ok");
//  }

//  public int down_from_list(){
//      var results = instanta.get_note_list().get_array_member ("data");
//      list.clear();
//      foreach (var geonode in results.get_elements ()) {
//              var geoname = geonode.get_object ();
//              if(geoname.get_int_member ("deleted")==0) {
//                      list.add (down_note(geoname.get_string_member ("key")));
//              }
//      }
//      print("ok");
//      foreach (Note i in list) {
//              print(i.key+"\n");
//      }
//      return 0;
//  }

public int sync_from_list(){
	uint return_code;
	var note_list=instanta.get_note_list(out return_code);
	if(return_code!=200) return 1;
	var results = note_list.get_array_member ("data");
	var id_list = new Gee.HashSet<string> ();
	int updated = 0;
	foreach (var geonode in results.get_elements ()) {
		var geoname = geonode.get_object ();
		//  if(geoname.get_int_member ("deleted")==0) {
		id_list.add(geoname.get_string_member ("key"));
		var itemno =get_note_id_by_key(geoname.get_string_member ("key"));
		var z=0;
		foreach (Note i in list) {
			if(i.modifydate>int64.parse(geoname.get_string_member ("modifydate").split(".")[0])) z++;
		}
		if(itemno==null) {list.insert (z,down_note(geoname.get_string_member ("key"))); updated++;}
		else{
			if (list[itemno].modifydate>int64.parse(geoname.get_string_member("modifydate").split(".")[0])) {update_note_cloud(list[itemno]); updated++;}
			if (list[itemno].modifydate<int64.parse(geoname.get_string_member("modifydate").split(".")[0])) {
				list.remove_at(itemno);
				list.insert (z,down_note(geoname.get_string_member ("key")));
				updated++;
			};
			if (list[itemno].modifydate==int64.parse(geoname.get_string_member("modifydate").split(".")[0])) {
				if(list[itemno].deleted!=geoname.get_int_member("deleted")) list[itemno].deleted = geoname.get_int_member("deleted");
			}
		}
		//  }
	}
	print("\n"+updated.to_string()+" updated.\n");

	for (int ii=0; ii< list.size; ii++) {
		var i=list[ii];
		if(id_list.contains(i.key)==false) {if (i.key.split(".")[0]=="temp") update_note_cloud(i); else list.remove(i); }
	}
	return 0;

}

public Note down_note(string noteid){
	var notamea=instanta.get_note(noteid);
	string key = notamea.get_string_member("key");
	string content = notamea.get_string_member("content");
	var modifydate=int64.parse(notamea.get_string_member ("modifydate").split(".")[0]);
	var createdate =int64.parse(notamea.get_string_member ("createdate").split(".")[0]);
	var tagsbuild = new StringBuilder ();
	notamea.get_array_member("tags").foreach_element((a,b,c) => {
			var tag=a.get_string_element(b);
			tagsbuild.append (tag);
			tagsbuild.append (" ");
		});
	var tags=tagsbuild.truncate(tagsbuild.len-1).str;
	int64 version = notamea.get_int_member("version");
	int64 deleted = notamea.get_int_member("deleted");
	print("Note with id "+key.to_string()+" downloaded.\n");
	return new Note(key,content, modifydate,createdate,tags,version,deleted);
}

public int ? get_note_id_by_key(string key){
	for(int i=0; i<list.size; i++) {
		if(list[i].key==key) return i;
	}
	return null;
}
public string json_for_update(Note notaa){
	Json.Builder builder = new Json.Builder ();
	builder.begin_object ();
	builder.set_member_name ("modifydate");
	builder.add_string_value (notaa.modifydate.to_string()+".000000");
	builder.set_member_name ("tags");
	builder.begin_array ();
	foreach (string y in notaa.tags.split(" ")) {
		var tag=y;
		builder.add_string_value (tag);
	}
	builder.end_array ();
	builder.set_member_name ("deleted");
	builder.add_int_value (notaa.deleted);
	builder.set_member_name ("createdate");
	builder.add_string_value (notaa.createdate.to_string()+".000000");
	builder.set_member_name ("systemtags");
	builder.begin_array ();
	builder.end_array ();
	builder.set_member_name ("content");
	builder.add_string_value (notaa.content);
	if(notaa.key.split(".")[0]!="temp") {
		builder.set_member_name ("key");
		builder.add_string_value (notaa.key);
	}
	builder.end_object ();
	Json.Generator generator = new Json.Generator ();
	Json.Node root = builder.get_root ();
	generator.set_root (root);
	string str = generator.to_data (null);
	//  print (str);
	//  print ("\n");
	return str;
}

public int update_note_cloud(Note nt){
	var update_results=instanta.update_note(nt.key, json_for_update(nt));
	Note i = list[list.index_of(nt)];
	i.key = update_results.get_string_member("key");
	i.modifydate=int64.parse(update_results.get_string_member("modifydate").split(".")[0]);
	i.createdate=int64.parse(update_results.get_string_member("createdate").split(".")[0]);
	var tagsbuild = new StringBuilder ();
	update_results.get_array_member("tags").foreach_element((a,b,c) => {
			var tag=a.get_string_element(b);
			tagsbuild.append (tag);
			tagsbuild.append (" ");
		});
	i.tags=tagsbuild.truncate(tagsbuild.len-1).str;
	i.version = update_results.get_int_member("version");
	print(i.key+" updated.\n");
	return 0;
}

public int update_note_local(Note i){
	string data = Json.gobject_to_data (i, null);
	var output = FileStream.open(datafolder+"/"+i.key+".file","w");
	output.printf(data);
	return 0;
}

public int delete_note_cloud(Note i){
	int ret = instanta.delete_note(i.key);
	list.remove(i);
	return ret;
}

public Note add_note(){
	var a = new Note.temp();
	list.insert (0, a);
	return a;
}
}