# Simpler Notes
[![pipeline status](https://gitlab.com/psnszsn/simpler-notes/badges/master/pipeline.svg)](https://gitlab.com/psnszsn/simpler-notes/commits/master)
Native linux client for Simplenote, built with Vala and Gtk.

## Install it from source
You can install Simpler Notes by compiling from source, here's the list of dependencies required:
 - `valac`
 - `meson`
 - `glib-2.0`
 - `gobject-2.0`
 - `libsoup-2.4`
 - `gee-0.8`
 - `json-glib-1.0`
 - `gtk+-3.0`

If you're using ubuntu or another debian-based distro, you can probably install all the dependencies with these commands:
```
apt update
apt install valac meson libsoup2.4-dev libjson-glib-dev libgee-0.8-dev libgtk-3-dev
```
## Building
```
meson builddir --prefix=/usr
cd builddir
ninja && sudo ninja install
```
